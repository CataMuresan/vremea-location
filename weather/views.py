import requests
from requests import get
from django.shortcuts import render

def index(request):
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=417884e2495672b9d82b5c6296fe8836'
    city = get('https://ipapi.co/city/').text
    #city = 'Las Vegas'

    r = requests.get(url.format(city)).json()
  
    city_weather = {
        'city' : city,
        'temperature' : r['main']['temp'],
        'description' : r['weather'][0]['description'],
        'icon' : r['weather'][0]['icon'],

    }

    context = {'city_weather' : city_weather}

    return render(request, 'weather/weather.html', context)